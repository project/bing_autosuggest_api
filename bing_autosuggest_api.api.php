<?php

/**
 * @file
 * Documentation for Bing Autosuggest API module hook(s).
 */

/**
 * Alter the suggestions data returned by the API.
 *
 * @param object $suggestions
 *   The original suggestions returned from the API.
 *
 *   The $suggestions are what is pushed back through to the front end. The
 *   script that used this data expects each suggestion to have a minimum of a
 *   'displayText', 'url' and 'cmsUrl' keys.
 */
function hook_bing_autosuggest_api_suggestions_alter(&$suggestions) {
  // Manipulate the 'modified' array values.
}