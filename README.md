# Bing Autosuggest API

Allows Drupal to integrate various forms with the Bing Autosuggest API, via
*customising* the built in Drupal autocomplete functionality.

## Overview

The general module usage is:

* Administrator selects which search form(s) to use Bing Autosuggest functionality, via the module `/admin` form [1]
* User visits site and types into search input
* Drupal autocomplete functionality sends a request to Drupal, on key presses [2]
* Drupal uses configured credentials to request data from the Bing Autosuggest API
* Logic in the Drupal module substitutes suggestion URLs from `bing.com` to the default configured Drupal site search URL and suggestion list (or a *No suggestions* message) is delivered back to the browser for display under the search input
* User clicks a suggestion link
* Query is sent to the search provider for the suggestion that the user clicked
* Search results for the suggestion, returned from the search provider are displayed to the user

[1] `/admin/config/search/bing_autosuggest_api`

[2] Drupal autocomplete limits the frequency with which the key presses send
requests, and, Bing has various - depending on account type - rate limits too

## Notes

You can see from the above description, the browser sends a request to Drupal
and then Drupal sends a request to the Bing Autosuggest API, a double hop.

Strictly the first request does not have to go through Drupal and could go
straight to the Autosuggest API.

But, if we *don't* go through Drupal we must:

* expose API keys into javascript
* handle API request/response headers in front end logic
* handle API related cookie(s) in front end logic
* handle API response suggestion URL substitution in front end logic
* lose the ability to easily log API (error) responses
* lose the ability to take advantage of Drupal IP blocking functionality

The trade off, of sending API requests via Drupal, is the above points versus
suggestion response latency.

Any approach that does not send request via Drupal will undoubtedly offer the
user quicker response times, which may or may not be supported depending on the
API subscription/pricing tier in use (the free tier is 1 request per second).

## Todo

Each request to Drupal has the overhead of bootstrapping. We could look at
using the JS module [3], that could enable us to only partially bootstrap
request, giving a potential performance boost.

[3] https://www.drupal.org/project/js

## Documentation

Obtain Bing Autosuggest API subscription key(s):

    https://azure.microsoft.com/en-gb/try/cognitive-services/?api=autosuggest-api

Bing Autosuggest API documentation:

    https://docs.microsoft.com/en-us/rest/api/cognitiveservices/bing-autosuggest-api-v7-reference

Use and display requirements:

    https://docs.microsoft.com/en-us/azure/cognitive-services/bing-autosuggest/useanddisplayrequirements
