<?php

/**
 * @file
 * Bing Autosuggest API /admin functionality.
 */

/**
 * Settings form for settings affecting Bing Autosuggest API requests.
 *
 * @return array
 *   Drupal form API compatible set of form elements.
 */
function bing_autosuggest_api_settings() {
  $elements = array();

  $elements['bing_autosuggest_api_api_parameter_settings'] = array(
    '#type' => 'fieldset',
    '#tree' => FALSE,
    '#title' => t('API parameter settings'),
  );

  $elements['bing_autosuggest_api_api_parameter_settings']['bing_autosuggest_api_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Bing Autosuggest API Endpoint'),
    '#description' => t('Enter the Bing API endpoint URL'),
    '#default_value' => variable_get('bing_autosuggest_api_endpoint', 'https://api.cognitive.microsoft.com/bing/v7.0/Suggestions'),
    '#required' => TRUE,
  );

  $subscription_key_form_select_options = array();
  $subscription_key_ids = bing_autosuggest_api_subscription_key_ids();

  for ($i = 0, $len = count($subscription_key_ids); $i < $len; $i++) {
    $subscription_key_id = $subscription_key_ids[$i];
    $one_based_index = ($i + 1);
    $key_display_value = t('Subscription key @index', array(
      '@index' => $one_based_index
    ));
    $subscription_key_form_select_options[$subscription_key_id] = $key_display_value;

    $elements['bing_autosuggest_api_api_parameter_settings'][$subscription_key_id] = array(
      '#type' => 'textfield',
      '#title' => $key_display_value,
      '#default_value' => variable_get($subscription_key_id, ''),
      '#maxlength' => 60,
      '#size' => 60,
      '#required' => ($one_based_index === 1) ? TRUE : FALSE,
      '#element_validate' => array('bing_autosuggest_api_element_validate_subscription_key'),
    );
  }

  $elements['bing_autosuggest_api_api_parameter_settings']['bing_autosuggest_api_active_subscription_key'] = array(
    '#type' => 'select',
    '#title' => t('Active subscription key'),
    '#description' => t('Specify which of the subscription keys specified above should be used for API requests'),
    '#options' => $subscription_key_form_select_options,
    '#default_value' => variable_get('bing_autosuggest_api_active_subscription_key', $subscription_key_ids[0]),
    '#required' => TRUE,
    '#element_validate' => array('bing_autosuggest_api_element_validate_active_subscription_key'),
  );

  $elements['bing_autosuggest_api_api_parameter_settings']['bing_autosuggest_api_endpoint_response_format'] = array(
    '#type' => 'select',
    '#title' => t('API response format'),
    '#description' => t('Specify the data format that the API will return'),
    '#options' => array(
      'application/json' => t('JSON'),
    ),
    '#default_value' => variable_get('bing_autosuggest_api_endpoint_response_format', 'application/json'),
  );

  $elements['bing_autosuggest_api_api_parameter_settings']['bing_autosuggest_api_pragma'] = array(
    '#type' => 'select',
    '#title' => t('Pragma (request header)'),
    '#description' => t('Prevent returning available cached content, the default, if required'),
    '#options' => array(
      'cache' => t('cache'),
      'no-cache' => t('no-cache'),
    ),
    '#default_value' => variable_get('bing_autosuggest_api_pragma', 'cache'),
  );

  $elements['bing_autosuggest_api_api_parameter_settings']['bing_autosuggest_api_market'] = array(
    '#type' => 'select',
    '#title' => t('Market'),
    '#description' => t('Market where results come from'),
    '#options' => array(
      'es-AR' => t('Argentina: Spanish'),
      'en-AU' => t('Australia: English'),
      'de-AT' => t('Austria: German'),
      'nl-BE' => t('Belgium: Dutch'),
      'fr-BE' => t('Belgium: French'),
      'pt-BR' => t('Brazil: Portuguese'),
      'en-CA' => t('Canada: English'),
      'fr-CA' => t('Canada: French'),
      'es-CL' => t('Chile: Spanish'),
      'da-DK' => t('Denmark: Danish'),
      'fi-FI' => t('Finland: Finnish'),
      'fr-FR' => t('France: French'),
      'de-DE' => t('Germany: German'),
      'zh-HK' => t('Hong Kong SAR: Traditional Chinese'),
      'en-IN' => t('India: English'),
      'en-ID' => t('Indonesia: English'),
      'it-IT' => t('Italy: Italian'),
      'ja-JP' => t('Japan: Japanese'),
      'ko-KR' => t('Korea: Korean'),
      'en-MY' => t('Malaysia: English'),
      'es-MX' => t('Mexico: Spanish'),
      'nl-NL' => t('Netherlands: Dutch'),
      'en-NZ' => t('New Zealand: English'),
      'no-NO' => t('Norway: Norwegian'),
      'zh-CN' => t("People's republic of China: Chinese"),
      'pl-PL' => t('Poland: Polish'),
      'pt-PT' => t('Portugal: Portuguese'),
      'en-PH' => t('Republic of the Philippines: English'),
      'ru-RU' => t('Russia: Russian'),
      'ar-SA' => t('Saudi Arabia: Arabic'),
      'en-ZA' => t('South Africa: English'),
      'es-ES' => t('Spain: Spanish'),
      'sv-SE' => t('Sweden: Swedish'),
      'fr-CH' => t('Switzerland: French'),
      'de-CH' => t('Switzerland: German'),
      'zh-TW' => t('Taiwan: Traditional Chinese'),
      'tr-TR' => t('Turkey: Turkish'),
      'en-GB' => t('United Kingdom: English'),
      'en-US' => t('United States: English'),
      'es-US' => t('United States: Spanish'),
    ),
    '#default_value' => variable_get('bing_autosuggest_api_market', 'en-GB'),
  );

  $elements['bing_autosuggest_api_api_parameter_settings']['bing_autosuggest_api_set_lang'] = array(
    '#type' => 'select',
    '#title' => t('UI language'),
    '#description' => t('Language to use for user interface strings'),
    '#options' => array(
      'AR' => t('Arabic'),
      'ZH' => t('Chinese'),
      'DA' => t('Danish'),
      'NL' => t('Dutch'),
      'EN' => t('English'),
      'FI' => t('Finnish'),
      'FR' => t('French'),
      'DE' => t('German'),
      'IT' => t('Italian'),
      'JA' => t('Japanese'),
      'KO' => t('Korean'),
      'NO' => t('Norwegian'),
      'PL' => t('Polish'),
      'PT' => t('Portuguese'),
      'RU' => t('Russian'),
      'ES' => t('Spanish'),
      'SV' => t('Swedish'),
      'TR' => t('Turkish'),
    ),
    '#default_value' => variable_get('bing_autosuggest_api_set_lang', 'EN'),
  );

  $elements['bing_autosuggest_api_misc_settings'] = array(
    '#type' => 'fieldset',
    '#tree' => FALSE,
    '#title' => t('Misc. settings'),
  );

  $elements['bing_autosuggest_api_misc_settings']['bing_autosuggest_api_proxy'] = array(
    '#type' => 'textfield',
    '#title' => t('Proxy server'),
    '#description' => t('Proxy for API requests, E.g. http://11.22.33.44:8080'),
    '#size' => 70,
    '#maxlength' => 100,
    '#default_value' => variable_get('bing_autosuggest_api_proxy', ''),
  );

  $elements['bing_autosuggest_api_misc_settings']['bing_autosuggest_api_request_delay'] = array(
    '#type' => 'select',
    '#title' => t('Minimum delay between requests to API'),
    '#description' => t("How long to delay each API request when typing in
      the form input control. Your subscription type determines the number of
      queries per second you're allowed to send."),
    '#options' => array(
      '0' => t('0 ms'),
      '50' => t('50 ms'),
      '100' => t('100 ms'),
      '150' => t('150 ms'),
      '200' => t('200 ms'),
      '250' => t('250 ms'),
      '300' => t('300 ms (Drupal autocomplete default)'),
      '350' => t('350 ms'),
      '400' => t('400 ms'),
      '450' => t('450 ms'),
      '500' => t('500 ms'),
      '550' => t('550 ms'),
      '600' => t('600 ms'),
      '650' => t('650 ms'),
      '700' => t('700 ms'),
      '750' => t('750 ms'),
      '800' => t('800 ms'),
      '850' => t('850 ms'),
      '900' => t('900 ms'),
      '950' => t('950 ms'),
      '1000' => t('1 s'),
    ),
    '#default_value' => variable_get('bing_autosuggest_api_request_delay', 300),
  );

  $elements['bing_autosuggest_api_misc_settings']['bing_autosuggest_api_message_no_suggestion'] = array(
    '#type' => 'textfield',
    '#title' => t('No suggestion(s) message'),
    '#description' => t('Text shown when no suggestions are received'),
    '#default_value' => variable_get('bing_autosuggest_api_message_no_suggestion', t('No suggestion')),
    '#required' => TRUE,
  );

  $elements['bing_autosuggest_api_misc_settings']['bing_autosuggest_api_alter_suggestion_url'] = array(
    '#type' => 'radios',
    '#title' => t('Suggestion URLs relative to (default) Drupal search, not
      Bing web search'),
    '#default_value' => variable_get('bing_autosuggest_api_alter_suggestion_url', 1),
    '#options' => array(
      1 => t('Yes'),
      0 => t('No'),
    ),
    '#description' => t("API response suggestion URLs point to bing.com. Select
      'Yes' here to substitute those links so they are relative to the Drupal
      (default) site search instead when displayed to the user. Eg. When
      'drupal' is the suggestion, instead of
      <em>https://www.bing.com/search?q=drupal</em> the suggestion becomes
      <em>http(s)://%domain/search/%default_search/drupal</em>",
      array(
        '%domain' => $_SERVER['SERVER_NAME'],
        '%default_search' => variable_get('search_default_module', 'node')
      )
    ),
  );

  $elements['bing_autosuggest_api_misc_settings']['bing_autosuggest_api_cookie_days_duration'] = array(
    '#type' => 'textfield',
    '#title' => t('Cookie days duration'),
    '#description' => t('Each request provides the
      <em>X-MSEdge-ClientID</em> header/value to "to provide users with
      consistent behavior across Bing API calls". The value is stored in a
      persistent cookie. This setting determines the length of time in days
      before the cookie expires.'),
    '#default_value' => variable_get('bing_autosuggest_api_cookie_days_duration', 30),
    '#required' => TRUE,
    '#element_validate' => array('element_validate_integer_positive'),
  );

  $elements['bing_autosuggest_api_debug_settings'] = array(
    '#type' => 'fieldset',
    '#tree' => FALSE,
    '#title' => t('Debug settings'),
  );

  $elements['bing_autosuggest_api_debug_settings']['bing_autosuggest_api_log_api_request_response'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log API request/response data'),
    '#description' => t('If performance of the suggestions appears slow, turning
      on this logging can identify what the performance of requests to the API
      endpoint are like, and where performance issues may be able to be
      addressed. <strong>NOTE: Logging itself will incur some performance
      overhead</strong>.'),
    '#default_value' => variable_get('bing_autosuggest_api_log_api_request_response', 0),
  );

  $elements['bing_autosuggest_api_autosuggest_form_ids'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('Forms using* Bing Autosuggest API functionality'),
    '#description' => t('*Also requires a <em>hook_form_FORM_ID_alter()</em> implementation. See bing_autosuggest_api_form_search_form_alter()'),
  );

  $default_form_list = bing_autosuggest_api_default_form_list();
  $form_list = variable_get('bing_autosuggest_api_autosuggest_form_ids', $default_form_list);

  foreach ($form_list as $form_id => $state) {
    $elements['bing_autosuggest_api_autosuggest_form_ids'][$form_id] = array(
      '#type' => 'checkbox',
      '#title' => $form_id,
      '#default_value' => $state === 0 ? FALSE : TRUE,
    );
  }

  return system_settings_form($elements);
}

/**
 * Validation of API key settings form element(s).
 *
 * If a subscription key field value is provided, it should be a 32 character
 * alphanumeric value.
 *
 * @todo Azure Cognitive Services sample code validated the key at 32 characters
 * but the API docs do not mention this as a constraint. Confirm correct.
 *
 * @param array $element
 *   Form element definition.
 * @param array $form_state
 *   Current form state.
 * @param array $form
 *   Form definition.
 */
function bing_autosuggest_api_element_validate_subscription_key($element, &$form_state, $form) {
  $is_required = $element['#required'] === TRUE ? TRUE : FALSE;
  $is_empty = empty($element['#value']);
  $is_valid_length = drupal_strlen($element['#value']) === 32 ? TRUE : FALSE;

  if (($is_required && !$is_valid_length) || (!$is_required && !$is_empty && !$is_valid_length)) {
    form_error($element, t("Invalid value for '@key_title'", array(
      '@key_title' => $element['#title'],
    )));
  }
}

/**
 * Validation of active subscription key settings form element.
 *
 * Don't allow the secondary key to be set as the active key, if there's been no
 * secondary key value been set.
 *
 * @param array $element
 *   Form element definition.
 * @param array $form_state
 *   Current form state.
 * @param array $form
 *   Form definition.
 */
function bing_autosuggest_api_element_validate_active_subscription_key($element, &$form_state, $form) {
  $subscription_key_ids = bing_autosuggest_api_subscription_key_ids();
  $secondary_key_id = $subscription_key_ids[1];

  if ($element['#value'] === $secondary_key_id && empty($form_state['values'][$secondary_key_id])) {
    $secondary_key_title = $form[$element['#array_parents'][0]][$secondary_key_id]['#title'];
    form_error($element, t("Unable to set '%field_title' as the active key. '%field_title' value is not set.", array('%field_title' => $secondary_key_title)));
  }
}

/**
 * Form form IDs of those that are by default able to use the Autosuggest API.
 *
 * @return array
 *   List of Drupal form IDs, that also have a hook_form_FORM_ID_alter()
 *   implementation that enables the Autosuggest API related scripts to be added
 *   tp them.
 *
 * @see Comment for bing_autosuggest_api_form_search_block_form_alter().
 */
function bing_autosuggest_api_default_form_list() {
  return array(
    'search_form' => 0,
    'search_block_form' => 0,
  );
}
