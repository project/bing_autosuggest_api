(function ($) {
  'use strict';

  /**
   * Define a namespace for module.
   */
  Drupal.bingAutosuggestApi = function() {};

  /**
   * Determine if a 'match' is actually the 'no suggestion' message.
   *
   * @param {Object} match
   *   Single 'match' from the autocomplete callback.
   *
   * @return {boolean}
   *   True if the 'match' is the 'no suggestion' message.
   */
  Drupal.jsAC.prototype.isNoSuggestionMatch = function (match) {
    return match.displayText === Drupal.settings.bingAutosuggestApi.noSuggestion.displayText;
  };

  /**
   * Determine if the request made was to the Bing Autosuggest API.
   *
   * The bing_autosuggest_api module returns a different 'matches' param data
   * structure.
   *
   * @param {Array} matches
   *   Results from the autocomplete callback.
   *
   * @return {boolean}
   *   Whether or not a request result was from the Autosuggest API.
   */
  Drupal.jsAC.prototype.isBingAsApiAc = function (matches) {
    // Our module always returns at least one 'match'. Even if API doesn't
    // return anything we send through a 'No suggestions' item.
    return matches.length > 0 && typeof matches[0].displayText !== 'undefined';
  };

  /**
   * Determine if the currently used control is for the Autosuggest API.
   *
   * The bing_autosuggesst_api adds a CSS class to the <input /> to allow us to
   * identify an autocomplete form control as one that's using the Autosuggest
   * API.
   *
   * @return {boolean}
   *   Whether or not the control is talking to the Bing Autosuggest API.
   */
  Drupal.ACDB.prototype.isBingAsApiAc = function () {
    return $(this.owner.input).hasClass('bing-autosuggest-api');
  };

  /**
   * Preserve/rename the original autocomplete 'search' function.
   */
  Drupal.ACDB.prototype.searchOriginal = Drupal.ACDB.prototype.search;

  /**
   * Redefine autocomplete 'search' behaviour.
   *
   * By doing this we're preserving the original autocomplete behaviour for the
   * standard Form API autocomplete controls and are also able to deal with our
   * new Bing Autosuggest API type autocomplete control(s).
   *
   * @param {String} searchString
   *   Current user entered string.
   */
  Drupal.ACDB.prototype.search = function (searchString) {
    if (this.isBingAsApiAc()) {
      this.searchBing(searchString);
    }
    else {
      this.searchOriginal(searchString);
    }
  };

  /**
   * Define 'search' behaviour for Bing Autosuggest API autocommplete controls.
   *
   * This does what the Drupal core version does, but allows us to affect the
   * minumum delay between API requests that's not possible in the original.
   *
   * @param {String} searchString
   *   Current user entered string.
   */
  Drupal.ACDB.prototype.searchBing = function (searchString) {
    var db = this;
    // This is one reason for the override. The 'delay' value is hard-coded in
    // the Drupal.ACDB constructor function. Ideally we'd subclass that and
    // reimplement the Drupal behavior, but we can't, as the value isn't even a
    // parameter to the constructor.
    this.delay = Drupal.settings.bingAutosuggestApi.delay;
    this.searchString = searchString;

    searchString = searchString.replace(/^\s+|\.{2,}\/|\s+$/g, '');
    if (searchString.length <= 0 || searchString.charAt(searchString.length - 1) == ',') {
      return;
    }

    if (this.cache[searchString]) {
      return this.owner.found(this.cache[searchString]);
    }

    if (this.timer) {
      clearTimeout(this.timer);
    }

    this.timer = setTimeout(function () {
      db.owner.setStatus('begin');

      $.ajax({
        type: 'GET',
        url: db.uri + '/' + Drupal.encodePath(searchString),
        dataType: 'json',
        success: function (matches) {
          if (typeof matches.status == 'undefined' || matches.status != 0) {
            if (db.cachable(matches)) {
              db.cache[searchString] = matches;
            }

            if (db.searchString == searchString) {
              db.owner.found(matches);
            }
            db.owner.setStatus('found');
          }
        },
        error: function (xmlhttp) {
          Drupal.displayAjaxError(Drupal.ajaxError(xmlhttp, db.uri));
        }
      });
    }, this.delay);
  };

  /**
   * Determine if we want to 'cache' the response suggestions.
   *
   * Avoids needless API queries to get the same result for a term.
   *
   * @param {Array} matches
   *   Results from the autocomplete callback.
   *
   * @return {boolean}
   *   If the list should be cached for the term, or not.
   */
  Drupal.ACDB.prototype.cachable = function (matches) {
    var noSuggestions = this.noSuggestions(matches);

    return !noSuggestions || noSuggestions && !this.apiErrorMask(matches);
  };

  /**
   * Determine if the response is the 'no suggestions' message.
   *
   * @param {Array} matches
   *   Results from the autocomplete callback.
   *
   * @return {boolean}
   *   If the response is the 'no suggestions' message, or not.
   */
  Drupal.ACDB.prototype.noSuggestions = function (matches) {
    return matches.length == 1 && matches[0].displayText === Drupal.settings.bingAutosuggestApi.noSuggestion.displayText;
  };

  /**
   * Determine if the 'no suggestions' message is an API error mask.
   *
   * @param {Array} matches
   *   Results from the autocomplete callback.
   *
   * @return {boolean}
   *   If the message is a mask for an API error, or not.
   */
  Drupal.ACDB.prototype.apiErrorMask = function (matches) {
    return typeof matches[0].errorMask !== 'undefined' && matches[0].errorMask === true;
  };

  /**
   * Preserve/rename the original autocomplete 'found' function.
   */
  Drupal.jsAC.prototype.foundOriginal = Drupal.jsAC.prototype.found;

  /**
   * Redefine autocomplete 'found' behaviour.
   *
   * By doing this we're preserving the original autocomplete behaviour for the
   * standard Form API autocomplete controls and are also able to deal with our
   * new Bing Autosuggest API type autocomplete control(s).
   *
   * @param {Array} matches
   *   Results from the autocomplete callback.
   */
  Drupal.jsAC.prototype.found = function (matches) {
    if (this.isBingAsApiAc(matches)) {
      this.foundBing(matches);
    }
    else {
      this.foundOriginal(matches);
    }
  };

  /**
   * Define 'found' behaviour for Bing Autosuggest API autocommplete controls.
   *
   * This does what the Drupal core version does, but uses our differently
   * formatted data structure to create a list of clickable links that will
   * search the Drupal site with the Bing provided suggestion. We also provide
   * a 'No suggestions' message when the Bing API gives no suggestions.
   *
   * @param {Array} matches
   *   Results from the autocomplete callback.
   */
  Drupal.jsAC.prototype.foundBing = function(matches) {
    if (!this.input.value.length) {
      return false;
    }

    var ul = $('<ul></ul>');
    var ac = this;
    var key;
    var urlKey = this.urlPropertyName();

    for (key in matches) {
      var markup = matches[key].displayText;
      var dataValue = '';

      if (!this.isNoSuggestionMatch(matches[key])) {
        markup = Drupal.theme.prototype.l(matches[key].displayText, matches[key][urlKey]);
        dataValue = matches[key].displayText;
      }

      $('<li></li>')
        .html($('<div></div>').html(markup))
        .mousedown(function () {ac.hidePopup(this);})
        .mouseover(function () {ac.highlight(this);})
        .mouseout(function () {ac.unhighlight(this);})
        .data('autocompleteValue', dataValue)
        .appendTo(ul);
    }

    if (this.popup) {
      if (ul.children().length) {
        $(this.popup).empty().append(ul).show();
        $(this.ariaLive).html(Drupal.t('Autocomplete popup'));
      }
      else {
        $(this.popup).css({visibility: 'hidden'});
        this.hidePopup();
      }
    }
  };

  /**
   * Determine which suggestion property is used for the suggestion URL value.
   *
   * @return {string}
   *   The name of the property to use to get the URL value from.
   */
  Drupal.jsAC.prototype.urlPropertyName = function () {
    var property = 'url';
    var alterUrls = Drupal.settings.bingAutosuggestApi.alterUrl;

    if (typeof alterUrls !== 'undefined' && alterUrls == 1) {
      property = 'cmsUrl';
    }

    return property;
  }

  /**
   * Theme function to theme links.
   *
   * @param {String} text
   *   Text to use in the anchor.
   * @param {String} path
   *   Value to use for the anchor href attribute.
   *
   * @return {String}
   *   An HTML anchor tag.
   */
  Drupal.theme.prototype.l = function (text, path) {
    return '<a href="' + path + '">' + text + '</a>';
  };

})(jQuery);
